import { getJSON, setJSON } from '../services/local-storage';

const NOTES_KEY = 'notes';

export default class NotesStore {
  constructor() {
    this.notes = getJSON(NOTES_KEY) || [];
  }

  fetch() {
    return getJSON(NOTES_KEY) || [];
  }

  createNote(note) {
    setJSON(NOTES_KEY, [ ...this.notes, note ]);
  }

  deleteNote(noteId) {
    const updatedNotes = this.notes.filter(({ id }) => id != noteId);
    setJSON(NOTES_KEY, updatedNotes);
  }
};