import TextRecognizer from '../services/text-recognizer';
import NotesStore from '../stores/notes-store';

import NotesList from './notes-list';


export default class App {
  static init() {
    return new App().init();
  }

  constructor() {
    this.button = document.querySelector('#record-button');

    this.lang = document.querySelector('#lang');

    this.notesList = new NotesList();

    this.notesList.render();
  }

  init() {
    this.button.addEventListener('click', async () => {
      const recognizer = new TextRecognizer(this.lang.value);

      try {
        const text = await recognizer.start();
        const store = new NotesStore();

        console.log('t', text);

        store.createNote({
          text,
          id: Date.now(),
          date: new Date()
        });

        this.notesList.render();

      } catch (err) {
        console.log(err);
      }
    });
  }
}