const SpeechRecognition = window.SpeechRecognition ? SpeechRecognition : webkitSpeechRecognition;

const DEFAULT_LANG = 'en_US';
const GRAMMAR = '#JSGF V1.0; grammar command; public <command> = record';

export default class TextRecognizer {
  constructor(lang = DEFAULT_LANG) {
    this.recognition = new SpeechRecognition();

    this.recognition.grammars = this.recognitionList;
    this.recognition.lang = lang;

    this.recognition.interimResults = false;
    this.recognition.maxAlternatives = 1;
  }

  get recognitionList() {
    const recognitionList = new webkitSpeechGrammarList();
    recognitionList.addFromString(GRAMMAR, 1);

    return recognitionList;
  }

  start() {
    this.recognition.start();

    return new Promise((resolve, reject) => {
      this.recognition.onresult = (event) => {
        const text = event.results[0][0].transcript;

        resolve(text);
      };

      this.recognition.onerror = (err) => {
        reject(err)
      };
    });
  }
}