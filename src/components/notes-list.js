import NotesStore from '../stores/notes-store';
import Note from './note';

export default class NotesList {
  constructor(notes) {
    this.store = new NotesStore();
    this.element = document.querySelector('#notes-list');
  }

  render() {
    const notes = this.store.fetch();

    this.element.innerHTML = notes.map(
      note => new Note(note).render()
    ).join('');
  }
}

