import App from './components/app';

window.addEventListener(
  'DOMContentLoaded',
  () => App.init()
);