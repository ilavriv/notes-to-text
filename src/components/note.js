

export default class Note {
  constructor(note) {
    this.note = note;
  }

  get date() {
    const { date } = this.note;

    return new Date(date).toLocaleString();
  }

  render() {
    const { text, id } = this.note;

    return `
      <div class="note card padding-20" data-id="${id}">
       <h3 class="text-success">${this.date}</h3>
       <p>${text}</p>
      </div>
    `;
  }
}