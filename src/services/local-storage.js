
export const getJSON = (key) => {
  try {
    return JSON.parse(window.localStorage[key]);
  } catch(err) {
    return null;
  }
}

export const setJSON = (key, obj) => {
  window.localStorage[key] = JSON.stringify(obj)
};